---
title: 'Operators'
---

#### Some Common Operators

|Operator|Description |Operation| Result
|-------|-------------------------------|---------------------------|------------------------|
| `+`   | Addition                      | `2 + 3`                   | 5                      |
| `-`   | Subtraction                   | `2 - 3`                   | -1                     |
| `*`   | Multiplication                | `6 * 2`                   | 12                     |
| `/`   | Division                      | `2 / 3`                   | 0.25                   |
| `div` | Integer Division              | `4 / 2`                   | 2                      |
| `%%`  | Divisibility                  | `6 %% 2`                  | True                   |
| `mod` | (Integer) Modulo              | `7 mod 2`                 | 1                      |
| `**`  | Exponentiation                | `2 ** 3`                  | 8                      |
| `==`  | Numeric equal                 | `2 == 3`                  | False                  |
| `!=`  | Numeric not equal             | `2 != 3`                  | True                   |
| `>=`  | Numeric greater than or equal | `4 >= 4`                  | True                   |
| `<=`  | Numeric less than or equal    | `2 <= 2`                  | True                   |
| `>`   | Numeric greater than          | `4 > 4`                   | False                  |
| `<`   | Numeric less than             | `2 < 2`                   | False                  |
| `<=>` | Numeric three-way comparator  | `5 <=> 3`                 | More                   |
| `eq`  | String equal                  | `'a' eq 'a'`              | True                   |
| `ne`  | String not equal              | `'b' ne 'a'`              | True                   |
| `ge`  | String greater than or equal  | `'z' ge 't'`              | True                   |
| `le`  | String less than or equal     | `'r' le 'r'`              | True                   |
| `gt`  | String greater than           | `'l' gt 'd'`              | True                   |
| `lt`  | String less than              | `'j' lt 'e'`              | False                  |
| `cmp` | String three-way comparator   | `'x' cpm 'y'`             | Less                   |
| `~`   | String concatenation          | `'H' ~ 'i'`               | 'Hi'                   |
| `x`   | String repetition             | `'a' x 2`                 | 'aa'                   |
| `~~`  | Smartmatch                    | `2 ~~ Int`                | True                   |
| `+`   | coerces to numeric value      | `+False`                  | 0                      |
| `~`   | coerces to string value       | `~5`                      | '5'                    |
| `?`   | coerces to boolean value      | `?()`                     | False                  |
| `..`  | Range constructor.            | `2..5`, `2^..5`, `2^..^5` | [2, 5], (2, 5], (2, 5) |

#### Metaoperators

A metaoperator is an operators that acts on other operators. The `[...]` is
known as the *reduction metaoperator* and provided with a list and an
operator, it applies the given operator successively to the list.

For example: 

|Reduction operator| Regular operation |Result
|---------------|-----------|-------|
| `[+] 1, 2, 3` | 1 + 2 + 3 | 6     |
| `[*] 1, 2, 3` | 1 * 2 * 3 | 6     |
| `[<] 1, 2, 3` | 1 < 2 <3  | True  |
| `[>] 1, 2, 3` | 1 > 2 > 3 | False |

---

> For the complete list of operators, go to https://docs.perl6.org/language/operators
