---
title: 'Hello World!'
---

#### Comments

Raku supports both single line and multi-line comments. Use a pound (`#`) 
symbol for single line comments and ``#```, followed by a bracketing
construct (`()`, `[]`, `{}`, etc.) for multi-line comments. The same syntax 
for multi-line comments can be used for embedded comments.

```
# This is a single line comment.

#`{
This is a multiline comment and I 
can write as many lines as I want.
This is another line.
}

for #`(each letter in) 'a'..'c' {
    say #`(it to the terminal; that's) $_;
    #`[The Old Wise called it literate programming. jk!]
}
```

```
$ perl6 comments.p6
a
b
c
```

#### Hello world!

Raku has several ways outputting text to the terminal. Some of the most
noteworthy are:

* `print` - prints to standard output and coerces non-string objects to strings
  (`Str`) by calling the method `.Str` on them. It doesn't add a trailing
  newline to its output.
  
* `put` - similar to `print` but adds a trailing newline to its output by
  calling `print-nl` on its argument.
  
* `say` - similar to `put` but uses the `.gist` method to print the string 
  representation of a given object.

* `note` - similar to `say` but it prints to standard error instead. It prints
  "Noted" if no arguments are passed to its subroutine form.

```perl6
print "Hello, world!\n"; # Or: print("Hello, world!");
say   "Hello, world!";   # Or: say("Hello, world!");
put   "Hello, world!";   # Or: put("Hello, world!");
note  "Hello, world!";   # Or: note("Hello, World!");
note;                    # No argument passed so it prints "Noted" to STDERR.

# Or in a more object oriented manner:
"Hello, world!\n".print;
"Hello, world!".put;
"Hello, world!".say;
"Hello, world!".note;
```

```
$ perl6 hello-world.p6
```

*STDOUT:*
```
Hello, world!
Hello, world!
Hello, world!
Hello, world!
Hello, world!
Hello, world!
```

*STDERR:*
```
Hello, world!
Noted
Hello, world!
```


