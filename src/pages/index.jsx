import React from "react"
import Helmet from "react-helmet"
import styled from "styled-components"

import SEO from "../components/SEO"
import config from "../../data/SiteConfig"
import PostListing from "../components/PostListing";
import Footer from "../components/Footer";

class Index extends React.Component {

  render() {
    const lessons = this.props.data.lessonsJson.chapters
    return (
      <div className="index-container">
        <Helmet title={config.siteTitle}/>
        <SEO postEdges={lessons}/>
        <main>
          <BodyContainer>
            <h1>Raku by Example</h1>

            <p>
              Raku is a highly capable, feature-rich programming
              language made for at least the next hundred years. Among its main
              features are: object orientation, functional programming primitives, 
              definable grammars, gradual typing, parallelism, concurrency, 
              and asynchrony, etc.
            </p>

            <p>
            The <strong>Raku</strong> programming language is also
            known as <strong>Perl 6</strong>. Both names can be used
            interchangeably. Use the one you prefer.
            </p>

            <p>
            <em>Raku by Example</em> is an introduction to the Raku programming
            language using annotated example code. It is intended for those
            already familiar with programming fundamentals.
            </p>

            <p>
              Inspired by <a href="https://gobyexample.com">Go By Example</a>.
            </p>

            <PostListing postEdges={lessons}/>
            <Footer/>
          </BodyContainer>
        </main>
      </div>
    );
  }
}

export default Index;

const BodyContainer = styled.div`
    width: 420px;
    margin: 0 auto;
    a {
      color: black;
    }
    ul {
      padding: 0;
      list-style-type: none
    }
    h1 {
      font-weight: 400;
    }
`

export const pageQuery = graphql`
  query MainQuery {
    lessonsJson{
      chapters{
        post{
          id
          childMarkdownRemark{
            fields{
              slug
            }
            frontmatter{
              title
            }
          }
        }
      }
    }
  }
`;
