module.exports = {
  blogPostDir: "posts",                // The name of directory that contains your posts.
  lessonsDir: "lessons",               // The name of the directory that contains lessons or docs.
  siteTitle: "Raku by Example",        // Site title.
  siteTitleAlt: "Learn Raku syntax with examples.", // Alternative site title for SEO.
  siteLogo: "/logos/logo-1024.png",                 // Logo used for SEO and manifest.
  siteUrl: "https://uzluisf.gitlab.io/",            // Domain of your website without pathPrefix.
  pathPrefix: `/raku-by-example`,
  siteDescription: "Learn Raku Syntax", // Website description used for RSS feeds/meta description tag.
  siteRss: "/rss.xml",                  // Path to the RSS file.
  userLinks: [
    {
      label: "Gitlab",
      url: "https://gitlab.com/uzluisf",
      iconClassName: "fa fa-gitlab"
    },
    {
      label: "Github",
      url: "https://github.com/uzluisf",
      iconClassName: "fa fa-github"
    },
  ]
};
